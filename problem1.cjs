const fs = require('fs');

// creating new directory

function createDirectory(directoryName) {

    const directoryPath = `${__dirname}/${directoryName}`;

    const createDirectoryPromise = new Promise((resolve, reject) => {

        fs.mkdir(directoryPath, (err) => {
            if (err) {
                reject(err);
            } else {
                resolve();
            }
        });
    });

    return createDirectoryPromise;
}

function createFile(directoryName, numberOfFiles) {

    let countFile = 1;

    function recursiveCreateFile(countFile) {

        return new Promise((resolve, reject) => {

            if (countFile < numberOfFiles) {

                const filePath = `${__dirname}/${directoryName}/file${countFile}.json`;

                const data = {
                    id: countFile,
                    name: `name${countFile}`,
                };

                fs.writeFile(filePath, JSON.stringify(data), (err) => {
                    if (err) {
                        reject(err);
                    } else {
                        console.log("File created");
                        recursiveCreateFile(countFile + 1)
                            .then(() => {
                                resolve();
                            })
                            .catch((err) => {
                                reject(err);
                            });
                    }
                });
            } else {
                resolve();
            }
        });
    }

    return new Promise((resolve, reject) => {
        recursiveCreateFile(countFile)
            .then(() => {
                resolve();
            })
            .catch((err) => {
                reject(err);
            });
    });
}

function deleteFile(directoryName, numberOfFiles) {

    let countFile = 1;

    function recursiveDeleteFile(countFile) {

        return new Promise((resolve, reject) => {

            if (countFile < numberOfFiles) {

                const filePath = `${__dirname}/${directoryName}/file${countFile}.json`;

                fs.unlink(filePath, (err) => {
                    if (err) {
                        reject(err);
                    } else {
                        console.log("File deleted");
                        recursiveDeleteFile(countFile + 1)
                            .then(() => {
                                resolve();
                            })
                            .catch((err) => {
                                reject(err);
                            });
                    }
                });
            } else {
                resolve();
            }
        });
    }

    return new Promise((resolve, reject) => {
        recursiveDeleteFile(countFile)
            .then(() => {
                resolve();
            })
            .catch((err) => {
                reject(err);
            });
    });
}

module.exports = { createDirectory, createFile, deleteFile };