const fs = require('fs');

// reading lipsum.txt file

function readFile() {

    const filePath = `${__dirname}/lipsum.txt`;

    return new Promise((resolve, reject) => {

        fs.readFile(filePath, "utf-8", (err, data) => {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        });
    });
}

// adding all new created file name in filenames.txt

function appendFilename(fileName) {

    const filenamesPath = `${__dirname}/filenames.txt`;

    return new Promise((resolve, reject) => {

        fs.appendFile(filenamesPath, `${fileName}\n`, (err) => {
            if (err) {
                reject(err);
            } else {
                console.log(`${fileName} written in filename.txt`);
                resolve();
            }
        });
    });
}

// converting all the contents into uppercase and storing it into new txt file

function upperCaseFile(fileName, fileContent) {

    const filePath = `${__dirname}/${fileName}`;
    const fileContentUpperCase = fileContent.toUpperCase();

    return new Promise((resolve, reject) => {

        fs.writeFile(filePath, fileContentUpperCase, (err) => {
            if (err) {
                reject(err);
            } else {
                console.log(`${fileName} created`);

                appendFilename(fileName)
                    .then(() => {
                        resolve(fileContentUpperCase);
                    })
                    .catch((err) => {
                        reject(err);
                    });
            }
        });
    });
}

// converting all the contents into lowercase, splitting it into new line and storing it into new txt file

function lowerCaseFile(fileName, fileContent) {

    const filePath = `${__dirname}/${fileName}`;
    const fileContentLowerCase = fileContent.toLowerCase();

    const splitLowerCaseFile = fileContentLowerCase
        .split(". ")
        .join(".\n");

    return new Promise((resolve, reject) => {

        fs.writeFile(filePath, splitLowerCaseFile, (err) => {
            if (err) {
                reject(err);
            } else {
                console.log(`${fileName} created`);

                appendFilename(fileName)
                    .then(() => {
                        resolve();
                    })
                    .catch((err) => {
                        reject(err);
                    });
            }
        });
    });
}

// sorting all the contents from the new files and storing it into new txt file

function sortFilesContent(fileName, readFileName1, readFileName2) {

    const filePath = `${__dirname}/`;

    return new Promise((resolve, reject) => {

        fs.readFile(filePath + readFileName1, "utf-8", (err1, fileData1) => {

            if (err1) {
                reject(err1);
            } else {

                return new Promise((resolve, reject) => {

                    fs.readFile(filePath + readFileName2, "utf-8", (err2, fileData2) => {

                        if (err2) {
                            reject(err2);
                        } else {

                            let allFileData = fileData1 + fileData2;
                            allFileData = allFileData
                                .replaceAll('\n', ' ')
                                .split(' ')
                                .sort((word1, word2) => {
                                    return word1.localeCompare(word2);
                                });

                            return new Promise((resolve, reject) => {

                                fs.writeFile(filePath + fileName, JSON.stringify(allFileData), (err3) => {
                                    if (err3) {
                                        reject(err3);
                                    } else {
                                        console.log(`${fileName} created`);

                                        appendFilename(fileName)
                                            .then(() => {
                                                resolve();
                                            })
                                            .catch((err) => {
                                                reject(err);
                                            });
                                    }
                                });
                            })
                                .then(() => {
                                    resolve();
                                })
                                .catch((err) => {
                                    reject(err);
                                });
                        }
                    });
                })
                    .then(() => {
                        resolve();
                    })
                    .catch((err) => {
                        reject(err);
                    });
            }
        });
    });
}

// deleting all the files according to the filename.txt

function deleteFiles() {

    filePath = `${__dirname}/filenames.txt`;

    return new Promise((resolve, reject) => {

        fs.readFile(filePath, "utf-8", (err, data) => {
            if (err) {
                reject(err);
            } else {

                const allFileName = data.split('\n');
                let index = 0;

                function recursiveDeleteFile(index, allFileName) {

                    return new Promise((resolve, reject) => {

                        if (index === allFileName.length - 1) {
                            resolve();
                        } else {
                            const filePath = `${__dirname}/${allFileName[index]}`;

                            fs.unlink(filePath, (error) => {
                                if (error) {
                                    reject(err);
                                } else {
                                    console.log(`${allFileName[index]} file deleted`);
                                }
                                recursiveDeleteFile(index + 1, allFileName)
                                    .then(() => {
                                        resolve();
                                    })
                                    .catch((err) => {
                                        reject(err);
                                    });
                            });
                        }
                    });
                }

                recursiveDeleteFile(index, allFileName)
                    .then(() => {
                        resolve();
                    })
                    .catch((err) => {
                        reject(err);
                    });
            }
        });
    });
}

module.exports = { readFile, upperCaseFile, lowerCaseFile, sortFilesContent, deleteFiles };