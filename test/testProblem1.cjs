const { createDirectory, createFile, deleteFile } = require("../problem1.cjs");

const directoryName = 'jsonFiles'; // new directory name
const randomNumber = Math.floor(Math.random() * 10) + 10; // Math.random() function to get random value between 10 to 20.

createDirectory(directoryName)
    .then(() => {
        console.log('Directory created');
        return createFile(directoryName, randomNumber); // function to create random JSON files
    })
    .then(() => {
        console.log("All files created");
        return deleteFile(directoryName, randomNumber); // function to delete random JSON files
    })
    .then(() => {
        console.log("All files deleted ");
    })
    .catch((err) => {
        console.error(err);
    });

